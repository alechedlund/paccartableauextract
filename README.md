**Written using VisualStudio 2019.  Requires an x64 developer command prompt in order to run 'nmake'.**

---

## Create an extract

1. Launch the x64 developer command prompt.
2. Change the working directory to the location of the Makefile on your local (eg 'cd C:\Users\ahedlund\Documents\paccartableauextract\FactEvent')
3. (optional) Type 'nmake' or 'nmake usage' and press enter to see usage
4. (optional) Type 'nmake clean' and press enter to delete the existing exe and obj, and rename any hyper files created from a previous build with a timestamp
	* You will want to do this if you made any changes to the FactEvent.cpp, or if there is an existing .hyper file that you do not want to extend.  Note that if the .hyper exists with the same name, it will not be overwritten, it will be appended to with the queried data.
5. (optional) Type 'nmake build' and press enter to build the exe and obj
6. (optional) Type 'nmake run' or 'nmake run ARGS="-h"' and press enter to see run commands
7. Type 'nmake run ARGS="-b -m YEARMONTH -f FILENAME"' to build the exe and obj and immediately run it to create the .hyper extract for the specified YEARMONTH (ie 201906) with a filename prefix of FILENAME
	* You may pass a comma separated list to -m variable to run multiple YEARMONTHs back to back (eg "201906,201907" will extract 201906 and then 201907)
	* If you don't pass the -m variable, the default YEARMONTH will be "0" (ie the entire past year)
	* If you don't pass the -f variable, the default FILENAME will be "FactEvent" (ie 'FactEvent_0.hyper')
