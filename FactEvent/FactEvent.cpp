#if defined(__APPLE__) && defined(__MACH__)
#include <TableauHyperExtract/TableauHyperExtract_cpp.h>
#else
#include "TableauHyperExtract_cpp.h"
#endif

#include <codecvt>
#include <cstring>
#include <iostream>
#include <sstream>
#include <locale>
#include <map>
#include <string>
#include <windows.h>
#include <odbcinst.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <sqlucode.h>

using namespace std;
using namespace Tableau;

//------------------------------------------------------------------------------
//  Display Usage
//------------------------------------------------------------------------------
void DisplayUsage()
{
    cerr << "USAGE: FactEvent [COMMAND] [OPTIONS]" << endl
		<< endl
		<< "COMMANDS:" << endl
		<< "  -h, --help           Show this help message and exit" << endl
		<< endl
		<< "  -b, --build          If a Tableau extract named FILENAME exists in the directory,"
		<< "                       extend it with data." << endl
		<< "                       If no Tableau extract named FILENAME exists in the directory,"
		<< "                       create one and populate it with data." << endl
		<< endl
		<< "OPTIONS:" << endl
		<< " -m YEARMONTH, --yearmonth YEARMONTH" << endl
		<< "                       YEARMONTH of the extract to be created or extended," << endl
		<< "                       can pass a comma separated list to run back to back." << endl
		<< "                       (default=0)" << endl
		<< " -f FILENAME, --filename FILENAME" << endl
		<< "                       FILENAME of the extract to be created or extended," << endl
		<< "                       a full filepath can be passed," << endl
		<< "                       Will be appended by _YEARMONTH.hyper" << endl
		<< "                       (default='<EXECUTABLE DIRECTORY>\FactEvent')" << endl
		<< endl;
}

//------------------------------------------------------------------------------
//  Parse Arguments
//------------------------------------------------------------------------------
//  Parse Command Line Arguments or Populate Defaults
//
//  Returns 'true' if all arguments are successfully parsed
//  Returns 'false' if there are any invalid arguments or no arguments
bool ParseArguments(int argc, char* argv[], map<string, wstring>& options)
{
    if (argc == 0)
    {
        return false;
    }

    wstring_convert<codecvt_utf8_utf16<wchar_t, 0x10ffff, codecvt_mode::little_endian>> converter;
    for (int i = 0; i < argc; ++i)
    {
        if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
        {
            options[string("help")] = wstring(L"true");
            return true;
        }
        else if (!strcmp(argv[i], "-b") || !strcmp(argv[i], "--build"))
        {
            options[string("build")] = wstring(L"true");
        }
		else if (!strcmp(argv[i], "-m") || !strcmp(argv[i], "--yearmonth"))
		{
			if (i >= argc)
			{
				return false;
			}
			options[string("yearmonth")] = converter.from_bytes(argv[++i]);
		}
        else if (!strcmp(argv[i], "-f") || !strcmp(argv[i], "--filename"))
        {
            if (i >= argc)
            {
                return false;
            }
            options[string("filename")] = converter.from_bytes(argv[++i]);
        }
        else
        {
            return false;
        }
    }

    //  Defaults
    //options["build"] = wstring(L"true");
	if (!options.count("yearmonth"))
	{
		options["yearmonth"] = L"0";
	}
	if (!options.count("filename"))
	{
		options["filename"] = L"FactEvent";
	}

    return true;
}

//------------------------------------------------------------------------------
//  Create or Open Extract
//------------------------------------------------------------------------------
shared_ptr<Extract> CreateExtract(const wstring& fileName)
{
    shared_ptr<Extract> extractPtr = nullptr;
    try
    {
        //  Create Extract Object
        //  (NOTE: TabExtractCreate() opens an existing extract with the given
        //   filename if one exists or creates a new extract with the given filename
        //   if one does not)
        extractPtr = make_shared<Extract>(fileName);

        //  Define `Extract` table
        auto tableName = L"Extract";
        if (!extractPtr->HasTable(tableName))
        {
            TableDefinition schema;
            schema.SetDefaultCollation(Collation_Binary);
			schema.AddColumn(L"EventID", Type_Integer);
			schema.AddColumn(L"VehicleID", Type_Integer);
			schema.AddColumn(L"CustomerID", Type_Integer);
			schema.AddColumn(L"CompanyID", Type_Integer);
			schema.AddColumn(L"SoftwareID", Type_Integer);
			schema.AddColumn(L"EngineFamilyID", Type_Integer);
			schema.AddColumn(L"StatusID", Type_Integer);
			schema.AddColumn(L"DateKey", Type_Integer);
			schema.AddColumn(L"TimeKey", Type_Integer);
			schema.AddColumn(L"MillisecondKey", Type_Integer);
			schema.AddColumn(L"SystemID", Type_Integer);
			schema.AddColumn(L"FaultID", Type_Integer);
			schema.AddColumn(L"LampID", Type_Integer);
			schema.AddColumn(L"EventTypeID", Type_Integer);
			schema.AddColumn(L"LocationID", Type_Integer);
			schema.AddColumn(L"Latitude", Type_Double);
			schema.AddColumn(L"Longitude", Type_Double);
			schema.AddColumn(L"TriggerTime", Type_DateTime);
			schema.AddColumn(L"Altitude", Type_Double);
			schema.AddColumn(L"Heading", Type_Integer);
			schema.AddColumn(L"PeakBusUtilization", Type_Integer);
			schema.AddColumn(L"TripId", Type_Integer);
			schema.AddColumn(L"CurrentBusUtilization", Type_Integer);
			schema.AddColumn(L"TotalSnapshots", Type_Integer);
			schema.AddColumn(L"ProtectLampOn", Type_Integer);
			schema.AddColumn(L"AmberWarningLampOn", Type_Integer);
			schema.AddColumn(L"RedStopLampOn", Type_Integer);
			schema.AddColumn(L"MalfunctionIndicatorLampOn", Type_Integer);
			schema.AddColumn(L"FlashProtectLampOn", Type_Integer);
			schema.AddColumn(L"FlashAmberWarningLampOn", Type_Integer);
			schema.AddColumn(L"FlashRedStopLampOn", Type_Integer);
			schema.AddColumn(L"FlashMalfunctionIndicatorLampOn", Type_Integer);
			schema.AddColumn(L"ConversionMethod", Type_Integer);
			schema.AddColumn(L"OccurrenceCount", Type_Integer);
			schema.AddColumn(L"PreTriggerSamples", Type_Integer);
			schema.AddColumn(L"PostTriggerSamples", Type_Integer);
			schema.AddColumn(L"AllLampsOnTime", Type_Double);
			schema.AddColumn(L"AmberLampCount", Type_Integer);
			schema.AddColumn(L"AmberLampTime", Type_Double);
			schema.AddColumn(L"RedLampCount", Type_Integer);
			schema.AddColumn(L"RedLampTime", Type_Double);
			schema.AddColumn(L"MilLampCount", Type_Integer);
			schema.AddColumn(L"MilLampTime", Type_Double);
			schema.AddColumn(L"EngineStartAmbient", Type_Double);
			schema.AddColumn(L"EngineStartCoolant", Type_Double);
			schema.AddColumn(L"TotalDistance", Type_Double);
			schema.AddColumn(L"TotalEngineHours", Type_Double);
			schema.AddColumn(L"TotalIdleFuel", Type_Double);
			schema.AddColumn(L"TotalIdleHours", Type_Double);
			schema.AddColumn(L"TotalFuel", Type_Double);
			schema.AddColumn(L"TotalPtoFuel", Type_Double);
			schema.AddColumn(L"TripDistance", Type_Double);
			schema.AddColumn(L"TripRunTime", Type_Double);
			schema.AddColumn(L"TripIdleTime", Type_Double);
			schema.AddColumn(L"IgnitionCycleCounter", Type_Integer);
			schema.AddColumn(L"ObdMonitorConditionsCounter", Type_Integer);
			schema.AddColumn(L"Dm20Time", Type_DateTime);
			schema.AddColumn(L"MessageID", Type_Integer);
			schema.AddColumn(L"CreatedDate", Type_DateTime);
			schema.AddColumn(L"LastModified", Type_DateTime);
			schema.AddColumn(L"ModemRSSI", Type_Integer);
			schema.AddColumn(L"GpsHorizontalAccuracy", Type_Double);
			schema.AddColumn(L"RadioMode", Type_UnicodeString);
			schema.AddColumn(L"RadioStatus", Type_UnicodeString);
			schema.AddColumn(L"GpsStatus", Type_UnicodeString);
			schema.AddColumn(L"OverallStatus", Type_UnicodeString);
			schema.AddColumn(L"GenDenominator", Type_Integer);
			schema.AddColumn(L"TripEventID", Type_UnicodeString);
			schema.AddColumn(L"FuelLevel", Type_Double);
			schema.AddColumn(L"TotalEnginePtoHours", Type_Double);
			schema.AddColumn(L"TotalRevolutions", Type_Integer);
			schema.AddColumn(L"TotalTransPtoHours", Type_Double);
			schema.AddColumn(L"VehicleSpeed", Type_Double);

            shared_ptr<Table> tablePtr = extractPtr->AddTable(tableName, schema);
            if (tablePtr == nullptr)
            {
                wcerr << L"A fatal error occurred while creating the new table" << endl
                           << L"Exiting Now." << endl;
                exit(EXIT_FAILURE);
            }
        }
    }
    catch (const TableauException& e)
    {
        wcerr << L"A fatal error occurred while creating the new extract: " << endl
                   << e.GetMessageT() << endl
                   << L"Exiting Now." << endl;
        exit(EXIT_FAILURE);
    }

    return extractPtr;
}

//------------------------------------------------------------------------------
//  Populate Extract
//------------------------------------------------------------------------------
void PopulateExtract(const shared_ptr<Extract> extractPtr, SQLINTEGER inpYearMonth)
{
    try
    {
        //  Populate `Extract` table
        {
            //  Get Schema
            auto tableName = L"Extract";
            shared_ptr<Table> tablePtr = extractPtr->OpenTable(tableName);
            shared_ptr<TableDefinition> schema = tablePtr->GetTableDefinition();

			//  Define handles and variables
			RETCODE retcode;
			SQLHANDLE sqlEnvHandle;
			SQLHANDLE sqlConnHandle;
			SQLHANDLE sqlStmtHandle;
			SQLWCHAR retconstring[2048];
			SQLBIGINT retEventID = 0;

			//  Initializations
			sqlConnHandle = NULL;
			sqlStmtHandle = NULL;

			//  Allocate environment handle
			retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &sqlEnvHandle);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "SQLAllocHandle(Environment) Failed\n";
				goto COMPLETED;
			}

			//  Set ODBC version to 3.0
			retcode = SQLSetEnvAttr(sqlEnvHandle, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "SQLSetEnvAttr(ODBC version) Failed\n";
				goto COMPLETED;
			}

			//  Allocate connection handles
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, sqlEnvHandle, &sqlConnHandle);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "SQLAllocHandle(ReadOnly Connection) Failed\n";
				goto COMPLETED;
			}

			//  Connect to SQL Server with ReadOnly
			cout << "\nAttempting ReadOnly connection to SQL Server...\n";
			retcode = SQLDriverConnectW(sqlConnHandle,
				NULL,
				(SQLWCHAR*)L"Driver={ODBC Driver 13 for SQL Server};Server=tcp:llfaq9fwjh.database.windows.net,1433;Database=paccar;Uid=vusiondba@llfaq9fwjh;Pwd=vusion!$23;Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;ApplicationIntent=ReadOnly;",
				SQL_NTS,
				retconstring,
				1024,
				NULL,
				SQL_DRIVER_NOPROMPT);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "Could not connect to SQL Server\n";
				goto COMPLETED;
			}
			else
			{
				cout << "Successfully connected to SQL Server\n";
			}

			//  Allocate statement handle
			retcode = SQLAllocHandle(SQL_HANDLE_STMT, sqlConnHandle, &sqlStmtHandle);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "SQLAllocHandle(Statement) Failed\n";
				goto COMPLETED;
			}

			//  Bind the first parameter (return code) to variable retEventID
			retcode = SQLBindParameter(sqlStmtHandle, 1, SQL_PARAM_OUTPUT, SQL_C_LONG, SQL_BIGINT, 0, 0, &retEventID, 0, NULL);
			if ((retcode != SQL_SUCCESS) && (retcode != SQL_SUCCESS_WITH_INFO))
			{
				printf("SQLBindParameter(retEventID) Failed\n\n");
				goto COMPLETED;
			}

			//  Bind the second parameter to variable inpYearMonth
			retcode = SQLBindParameter(sqlStmtHandle, 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, 0, 0, &inpYearMonth, 0, NULL);
			if ((retcode != SQL_SUCCESS) && (retcode != SQL_SUCCESS_WITH_INFO))
			{
				printf("SQLBindParameter(inpYearMonth) Failed\n\n");
				goto COMPLETED;
			}

			//  Execute the command
			cout << "Selecting FactEvent...\n";
			retcode = SQLExecDirectW(sqlStmtHandle, (SQLWCHAR*)L"{? = CALL tde.FactEvent_Select (?)}", SQL_NTS);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "Error querying SQL Server\n";
				goto COMPLETED;
			}
			else
			{
				//  Declare output variables and pointers
				SQLBIGINT EventID;                              SQLLEN lenEventID;
				SQLINTEGER VehicleID;                           SQLLEN lenVehicleID;
				SQLINTEGER CustomerID;                          SQLLEN lenCustomerID;
				SQLSMALLINT CompanyID;                          SQLLEN lenCompanyID;
				SQLSMALLINT SoftwareID;                         SQLLEN lenSoftwareID;
				SQLSMALLINT EngineFamilyID;                     SQLLEN lenEngineFamilyID;
				SQLSMALLINT StatusID;                           SQLLEN lenStatusID;
				SQLINTEGER DateKey;                             SQLLEN lenDateKey;
				SQLINTEGER TimeKey;                             SQLLEN lenTimeKey;
				SQLSMALLINT MillisecondKey;                     SQLLEN lenMillisecondKey;
				SQLSMALLINT SystemID;                           SQLLEN lenSystemID;
				SQLINTEGER FaultID;                             SQLLEN lenFaultID;
				SQLSMALLINT LampID;                             SQLLEN lenLampID;
				SQLSMALLINT EventTypeID;                        SQLLEN lenEventTypeID;
				SQLINTEGER LocationID;                          SQLLEN lenLocationID;
				SQLFLOAT Latitude;                              SQLLEN lenLatitude;
				SQLFLOAT Longitude;                             SQLLEN lenLongitude;
				SQL_TIMESTAMP_STRUCT TriggerTime;               SQLLEN lenTriggerTime;
				SQLFLOAT Altitude;                              SQLLEN lenAltitude;
				SQLBIGINT Heading;                              SQLLEN lenHeading;
				SQLINTEGER PeakBusUtilization;                  SQLLEN lenPeakBusUtilization;
				SQLBIGINT TripId;                               SQLLEN lenTripId;
				SQLBIGINT CurrentBusUtilization;                SQLLEN lenCurrentBusUtilization;
				SQLINTEGER TotalSnapshots;                      SQLLEN lenTotalSnapshots;
				SQLSMALLINT ProtectLampOn;                      SQLLEN lenProtectLampOn;
				SQLSMALLINT AmberWarningLampOn;                 SQLLEN lenAmberWarningLampOn;
				SQLSMALLINT RedStopLampOn;                      SQLLEN lenRedStopLampOn;
				SQLSMALLINT MalfunctionIndicatorLampOn;         SQLLEN lenMalfunctionIndicatorLampOn;
				SQLSMALLINT FlashProtectLampOn;                 SQLLEN lenFlashProtectLampOn;
				SQLSMALLINT FlashAmberWarningLampOn;            SQLLEN lenFlashAmberWarningLampOn;
				SQLSMALLINT FlashRedStopLampOn;                 SQLLEN lenFlashRedStopLampOn;
				SQLSMALLINT FlashMalfunctionIndicatorLampOn;	SQLLEN lenFlashMalfunctionIndicatorLampOn;
				SQLINTEGER ConversionMethod;					SQLLEN lenConversionMethod;
				SQLINTEGER OccurrenceCount;                     SQLLEN lenOccurrenceCount;
				SQLINTEGER PreTriggerSamples;                   SQLLEN lenPreTriggerSamples;
				SQLINTEGER PostTriggerSamples;                  SQLLEN lenPostTriggerSamples;
				SQLFLOAT AllLampsOnTime;                        SQLLEN lenAllLampsOnTime;
				SQLINTEGER AmberLampCount;                      SQLLEN lenAmberLampCount;
				SQLFLOAT AmberLampTime;                         SQLLEN lenAmberLampTime;
				SQLINTEGER RedLampCount;                        SQLLEN lenRedLampCount;
				SQLFLOAT RedLampTime;                           SQLLEN lenRedLampTime;
				SQLINTEGER MilLampCount;                        SQLLEN lenMilLampCount;
				SQLFLOAT MilLampTime;                           SQLLEN lenMilLampTime;
				SQLFLOAT EngineStartAmbient;                    SQLLEN lenEngineStartAmbient;
				SQLFLOAT EngineStartCoolant;                    SQLLEN lenEngineStartCoolant;
				SQLFLOAT TotalDistance;                         SQLLEN lenTotalDistance;
				SQLFLOAT TotalEngineHours;                      SQLLEN lenTotalEngineHours;
				SQLFLOAT TotalIdleFuel;                         SQLLEN lenTotalIdleFuel;
				SQLFLOAT TotalIdleHours;                        SQLLEN lenTotalIdleHours;
				SQLFLOAT TotalFuel;                             SQLLEN lenTotalFuel;
				SQLFLOAT TotalPtoFuel;                          SQLLEN lenTotalPtoFuel;
				SQLFLOAT TripDistance;                          SQLLEN lenTripDistance;
				SQLFLOAT TripRunTime;                           SQLLEN lenTripRunTime;
				SQLFLOAT TripIdleTime;                          SQLLEN lenTripIdleTime;
				SQLINTEGER IgnitionCycleCounter;                SQLLEN lenIgnitionCycleCounter;
				SQLINTEGER ObdMonitorConditionsCounter;         SQLLEN lenObdMonitorConditionsCounter;
				SQL_TIMESTAMP_STRUCT Dm20Time;                  SQLLEN lenDm20Time;
				SQLINTEGER MessageID;                           SQLLEN lenMessageID;
				SQL_TIMESTAMP_STRUCT CreatedDate;               SQLLEN lenCreatedDate;
				SQL_TIMESTAMP_STRUCT LastModified;              SQLLEN lenLastModified;
				SQLINTEGER ModemRSSI;                           SQLLEN lenModemRSSI;
				SQLFLOAT GpsHorizontalAccuracy;                 SQLLEN lenGpsHorizontalAccuracy;
				SQLWCHAR RadioMode[50];                         SQLLEN lenRadioMode;
				SQLWCHAR RadioStatus[50];                       SQLLEN lenRadioStatus;
				SQLWCHAR GpsStatus[50];                         SQLLEN lenGpsStatus;
				SQLWCHAR OverallStatus[50];                     SQLLEN lenOverallStatus;
				SQLINTEGER GenDenominator;                      SQLLEN lenGenDenominator;
				SQLWCHAR TripEventID[50];                       SQLLEN lenTripEventID;
				SQLFLOAT FuelLevel;                             SQLLEN lenFuelLevel;
				SQLFLOAT TotalEnginePtoHours;                   SQLLEN lenTotalEnginePtoHours;
				SQLINTEGER TotalRevolutions;                    SQLLEN lenTotalRevolutions;
				SQLFLOAT TotalTransPtoHours;                    SQLLEN lenTotalTransPtoHours;
				SQLFLOAT VehicleSpeed;                          SQLLEN lenVehicleSpeed;

				//  Fetch Data
				while (retcode = SQLFetch(sqlStmtHandle) == SQL_SUCCESS)
				{
					//  Set variable values
					SQLGetData(sqlStmtHandle, 1, SQL_C_DEFAULT, &EventID, sizeof(EventID), &lenEventID);
					SQLGetData(sqlStmtHandle, 2, SQL_C_DEFAULT, &VehicleID, sizeof(VehicleID), &lenVehicleID);
					SQLGetData(sqlStmtHandle, 3, SQL_C_DEFAULT, &CustomerID, sizeof(CustomerID), &lenCustomerID);
					SQLGetData(sqlStmtHandle, 4, SQL_C_DEFAULT, &CompanyID, sizeof(CompanyID), &lenCompanyID);
					SQLGetData(sqlStmtHandle, 5, SQL_C_DEFAULT, &SoftwareID, sizeof(SoftwareID), &lenSoftwareID);
					SQLGetData(sqlStmtHandle, 6, SQL_C_DEFAULT, &EngineFamilyID, sizeof(EngineFamilyID), &lenEngineFamilyID);
					SQLGetData(sqlStmtHandle, 7, SQL_C_DEFAULT, &StatusID, sizeof(StatusID), &lenStatusID);
					SQLGetData(sqlStmtHandle, 8, SQL_C_DEFAULT, &DateKey, sizeof(DateKey), &lenDateKey);
					SQLGetData(sqlStmtHandle, 9, SQL_C_DEFAULT, &TimeKey, sizeof(TimeKey), &lenTimeKey);
					SQLGetData(sqlStmtHandle, 10, SQL_C_DEFAULT, &MillisecondKey, sizeof(MillisecondKey), &lenMillisecondKey);
					SQLGetData(sqlStmtHandle, 11, SQL_C_DEFAULT, &SystemID, sizeof(SystemID), &lenSystemID);
					SQLGetData(sqlStmtHandle, 12, SQL_C_DEFAULT, &FaultID, sizeof(FaultID), &lenFaultID);
					SQLGetData(sqlStmtHandle, 13, SQL_C_DEFAULT, &LampID, sizeof(LampID), &lenLampID);
					SQLGetData(sqlStmtHandle, 14, SQL_C_DEFAULT, &EventTypeID, sizeof(EventTypeID), &lenEventTypeID);
					SQLGetData(sqlStmtHandle, 15, SQL_C_DEFAULT, &LocationID, sizeof(LocationID), &lenLocationID);
					SQLGetData(sqlStmtHandle, 16, SQL_C_DEFAULT, &Latitude, sizeof(Latitude), &lenLatitude);
					SQLGetData(sqlStmtHandle, 17, SQL_C_DEFAULT, &Longitude, sizeof(Longitude), &lenLongitude);
					SQLGetData(sqlStmtHandle, 18, SQL_C_DEFAULT, &TriggerTime, sizeof(TriggerTime), &lenTriggerTime);
					SQLGetData(sqlStmtHandle, 19, SQL_C_DEFAULT, &Altitude, sizeof(Altitude), &lenAltitude);
					SQLGetData(sqlStmtHandle, 20, SQL_C_DEFAULT, &Heading, sizeof(Heading), &lenHeading);
					SQLGetData(sqlStmtHandle, 21, SQL_C_DEFAULT, &PeakBusUtilization, sizeof(PeakBusUtilization), &lenPeakBusUtilization);
					SQLGetData(sqlStmtHandle, 22, SQL_C_DEFAULT, &TripId, sizeof(TripId), &lenTripId);
					SQLGetData(sqlStmtHandle, 23, SQL_C_DEFAULT, &CurrentBusUtilization, sizeof(CurrentBusUtilization), &lenCurrentBusUtilization);
					SQLGetData(sqlStmtHandle, 24, SQL_C_DEFAULT, &TotalSnapshots, sizeof(TotalSnapshots), &lenTotalSnapshots);
					SQLGetData(sqlStmtHandle, 25, SQL_C_DEFAULT, &ProtectLampOn, sizeof(ProtectLampOn), &lenProtectLampOn);
					SQLGetData(sqlStmtHandle, 26, SQL_C_DEFAULT, &AmberWarningLampOn, sizeof(AmberWarningLampOn), &lenAmberWarningLampOn);
					SQLGetData(sqlStmtHandle, 27, SQL_C_DEFAULT, &RedStopLampOn, sizeof(RedStopLampOn), &lenRedStopLampOn);
					SQLGetData(sqlStmtHandle, 28, SQL_C_DEFAULT, &MalfunctionIndicatorLampOn, sizeof(MalfunctionIndicatorLampOn), &lenMalfunctionIndicatorLampOn);
					SQLGetData(sqlStmtHandle, 29, SQL_C_DEFAULT, &FlashProtectLampOn, sizeof(FlashProtectLampOn), &lenFlashProtectLampOn);
					SQLGetData(sqlStmtHandle, 30, SQL_C_DEFAULT, &FlashAmberWarningLampOn, sizeof(FlashAmberWarningLampOn), &lenFlashAmberWarningLampOn);
					SQLGetData(sqlStmtHandle, 31, SQL_C_DEFAULT, &FlashRedStopLampOn, sizeof(FlashRedStopLampOn), &lenFlashRedStopLampOn);
					SQLGetData(sqlStmtHandle, 32, SQL_C_DEFAULT, &FlashMalfunctionIndicatorLampOn, sizeof(FlashMalfunctionIndicatorLampOn), &lenFlashMalfunctionIndicatorLampOn);
					SQLGetData(sqlStmtHandle, 33, SQL_C_DEFAULT, &ConversionMethod, sizeof(ConversionMethod), &lenConversionMethod);
					SQLGetData(sqlStmtHandle, 34, SQL_C_DEFAULT, &OccurrenceCount, sizeof(OccurrenceCount), &lenOccurrenceCount);
					SQLGetData(sqlStmtHandle, 35, SQL_C_DEFAULT, &PreTriggerSamples, sizeof(PreTriggerSamples), &lenPreTriggerSamples);
					SQLGetData(sqlStmtHandle, 36, SQL_C_DEFAULT, &PostTriggerSamples, sizeof(PostTriggerSamples), &lenPostTriggerSamples);
					SQLGetData(sqlStmtHandle, 37, SQL_C_DEFAULT, &AllLampsOnTime, sizeof(AllLampsOnTime), &lenAllLampsOnTime);
					SQLGetData(sqlStmtHandle, 38, SQL_C_DEFAULT, &AmberLampCount, sizeof(AmberLampCount), &lenAmberLampCount);
					SQLGetData(sqlStmtHandle, 39, SQL_C_DEFAULT, &AmberLampTime, sizeof(AmberLampTime), &lenAmberLampTime);
					SQLGetData(sqlStmtHandle, 40, SQL_C_DEFAULT, &RedLampCount, sizeof(RedLampCount), &lenRedLampCount);
					SQLGetData(sqlStmtHandle, 41, SQL_C_DEFAULT, &RedLampTime, sizeof(RedLampTime), &lenRedLampTime);
					SQLGetData(sqlStmtHandle, 42, SQL_C_DEFAULT, &MilLampCount, sizeof(MilLampCount), &lenMilLampCount);
					SQLGetData(sqlStmtHandle, 43, SQL_C_DEFAULT, &MilLampTime, sizeof(MilLampTime), &lenMilLampTime);
					SQLGetData(sqlStmtHandle, 44, SQL_C_DEFAULT, &EngineStartAmbient, sizeof(EngineStartAmbient), &lenEngineStartAmbient);
					SQLGetData(sqlStmtHandle, 45, SQL_C_DEFAULT, &EngineStartCoolant, sizeof(EngineStartCoolant), &lenEngineStartCoolant);
					SQLGetData(sqlStmtHandle, 46, SQL_C_DEFAULT, &TotalDistance, sizeof(TotalDistance), &lenTotalDistance);
					SQLGetData(sqlStmtHandle, 47, SQL_C_DEFAULT, &TotalEngineHours, sizeof(TotalEngineHours), &lenTotalEngineHours);
					SQLGetData(sqlStmtHandle, 48, SQL_C_DEFAULT, &TotalIdleFuel, sizeof(TotalIdleFuel), &lenTotalIdleFuel);
					SQLGetData(sqlStmtHandle, 49, SQL_C_DEFAULT, &TotalIdleHours, sizeof(TotalIdleHours), &lenTotalIdleHours);
					SQLGetData(sqlStmtHandle, 50, SQL_C_DEFAULT, &TotalFuel, sizeof(TotalFuel), &lenTotalFuel);
					SQLGetData(sqlStmtHandle, 51, SQL_C_DEFAULT, &TotalPtoFuel, sizeof(TotalPtoFuel), &lenTotalPtoFuel);
					SQLGetData(sqlStmtHandle, 52, SQL_C_DEFAULT, &TripDistance, sizeof(TripDistance), &lenTripDistance);
					SQLGetData(sqlStmtHandle, 53, SQL_C_DEFAULT, &TripRunTime, sizeof(TripRunTime), &lenTripRunTime);
					SQLGetData(sqlStmtHandle, 54, SQL_C_DEFAULT, &TripIdleTime, sizeof(TripIdleTime), &lenTripIdleTime);
					SQLGetData(sqlStmtHandle, 55, SQL_C_DEFAULT, &IgnitionCycleCounter, sizeof(IgnitionCycleCounter), &lenIgnitionCycleCounter);
					SQLGetData(sqlStmtHandle, 56, SQL_C_DEFAULT, &ObdMonitorConditionsCounter, sizeof(ObdMonitorConditionsCounter), &lenObdMonitorConditionsCounter);
					SQLGetData(sqlStmtHandle, 57, SQL_C_DEFAULT, &Dm20Time, sizeof(Dm20Time), &lenDm20Time);
					SQLGetData(sqlStmtHandle, 58, SQL_C_DEFAULT, &MessageID, sizeof(MessageID), &lenMessageID);
					SQLGetData(sqlStmtHandle, 59, SQL_C_DEFAULT, &CreatedDate, sizeof(CreatedDate), &lenCreatedDate);
					SQLGetData(sqlStmtHandle, 60, SQL_C_DEFAULT, &LastModified, sizeof(LastModified), &lenLastModified);
					SQLGetData(sqlStmtHandle, 61, SQL_C_DEFAULT, &ModemRSSI, sizeof(ModemRSSI), &lenModemRSSI);
					SQLGetData(sqlStmtHandle, 62, SQL_C_DEFAULT, &GpsHorizontalAccuracy, sizeof(GpsHorizontalAccuracy), &lenGpsHorizontalAccuracy);
					SQLGetData(sqlStmtHandle, 63, SQL_C_WCHAR, &RadioMode, sizeof(RadioMode), &lenRadioMode);
					SQLGetData(sqlStmtHandle, 64, SQL_C_WCHAR, &RadioStatus, sizeof(RadioStatus), &lenRadioStatus);
					SQLGetData(sqlStmtHandle, 65, SQL_C_WCHAR, &GpsStatus, sizeof(GpsStatus), &lenGpsStatus);
					SQLGetData(sqlStmtHandle, 66, SQL_C_WCHAR, &OverallStatus, sizeof(OverallStatus), &lenOverallStatus);
					SQLGetData(sqlStmtHandle, 67, SQL_C_DEFAULT, &GenDenominator, sizeof(GenDenominator), &lenGenDenominator);
					SQLGetData(sqlStmtHandle, 68, SQL_C_WCHAR, &TripEventID, sizeof(TripEventID), &lenTripEventID);
					SQLGetData(sqlStmtHandle, 69, SQL_C_DEFAULT, &FuelLevel, sizeof(FuelLevel), &lenFuelLevel);
					SQLGetData(sqlStmtHandle, 70, SQL_C_DEFAULT, &TotalEnginePtoHours, sizeof(TotalEnginePtoHours), &lenTotalEnginePtoHours);
					SQLGetData(sqlStmtHandle, 71, SQL_C_DEFAULT, &TotalRevolutions, sizeof(TotalRevolutions), &lenTotalRevolutions);
					SQLGetData(sqlStmtHandle, 72, SQL_C_DEFAULT, &TotalTransPtoHours, sizeof(TotalTransPtoHours), &lenTotalTransPtoHours);
					SQLGetData(sqlStmtHandle, 73, SQL_C_DEFAULT, &VehicleSpeed, sizeof(VehicleSpeed), &lenVehicleSpeed);

					//  Insert Data
					Row row(*schema);
					if (-1 != lenEventID)							row.SetLongInteger(0, EventID);
					if (-1 != lenVehicleID)							row.SetInteger(1, VehicleID);
					if (-1 != lenCustomerID)						row.SetInteger(2, CustomerID);
					if (-1 != lenCompanyID)							row.SetInteger(3, CompanyID);
					if (-1 != lenSoftwareID)						row.SetInteger(4, SoftwareID);
					if (-1 != lenEngineFamilyID)					row.SetInteger(5, EngineFamilyID);
					if (-1 != lenStatusID)							row.SetInteger(6, StatusID);
					if (-1 != lenDateKey)							row.SetInteger(7, DateKey);
					if (-1 != lenTimeKey)							row.SetInteger(8, TimeKey);
					if (-1 != lenMillisecondKey)					row.SetInteger(9, MillisecondKey);
					if (-1 != lenSystemID)							row.SetInteger(10, SystemID);
					if (-1 != lenFaultID)							row.SetInteger(11, FaultID);
					if (-1 != lenLampID)							row.SetInteger(12, LampID);
					if (-1 != lenEventTypeID)						row.SetInteger(13, EventTypeID);
					if (-1 != lenLocationID)						row.SetInteger(14, LocationID);
					if (-1 != lenLatitude)							row.SetDouble(15, Latitude);
					if (-1 != lenLongitude)							row.SetDouble(16, Longitude);
					if (-1 != lenTriggerTime)						row.SetDateTime(17, TriggerTime.year, TriggerTime.month, TriggerTime.day, TriggerTime.hour, TriggerTime.minute, TriggerTime.second, TriggerTime.fraction/1000000);
					if (-1 != lenAltitude)							row.SetDouble(18, Altitude);
					if (-1 != lenHeading)							row.SetLongInteger(19, Heading);
					if (-1 != lenPeakBusUtilization)				row.SetInteger(20, PeakBusUtilization);
					if (-1 != lenTripId)							row.SetLongInteger(21, TripId);
					if (-1 != lenCurrentBusUtilization)				row.SetLongInteger(22, CurrentBusUtilization);
					if (-1 != lenTotalSnapshots)					row.SetInteger(23, TotalSnapshots);
					if (-1 != lenProtectLampOn)						row.SetInteger(24, ProtectLampOn);
					if (-1 != lenAmberWarningLampOn)				row.SetInteger(25, AmberWarningLampOn);
					if (-1 != lenRedStopLampOn)						row.SetInteger(26, RedStopLampOn);
					if (-1 != lenMalfunctionIndicatorLampOn)		row.SetInteger(27, MalfunctionIndicatorLampOn);
					if (-1 != lenFlashProtectLampOn)				row.SetInteger(28, FlashProtectLampOn);
					if (-1 != lenFlashAmberWarningLampOn)			row.SetInteger(29, FlashAmberWarningLampOn);
					if (-1 != lenFlashRedStopLampOn)				row.SetInteger(30, FlashRedStopLampOn);
					if (-1 != lenFlashMalfunctionIndicatorLampOn)	row.SetInteger(31, FlashMalfunctionIndicatorLampOn);
					if (-1 != lenConversionMethod)					row.SetInteger(32, ConversionMethod);
					if (-1 != lenOccurrenceCount)					row.SetInteger(33, OccurrenceCount);
					if (-1 != lenPreTriggerSamples)					row.SetInteger(34, PreTriggerSamples);
					if (-1 != lenPostTriggerSamples)				row.SetInteger(35, PostTriggerSamples);
					if (-1 != lenAllLampsOnTime)					row.SetDouble(36, AllLampsOnTime);
					if (-1 != lenAmberLampCount)					row.SetInteger(37, AmberLampCount);
					if (-1 != lenAmberLampTime)						row.SetDouble(38, AmberLampTime);
					if (-1 != lenRedLampCount)						row.SetInteger(39, RedLampCount);
					if (-1 != lenRedLampTime)						row.SetDouble(40, RedLampTime);
					if (-1 != lenMilLampCount)						row.SetInteger(41, MilLampCount);
					if (-1 != lenMilLampTime)						row.SetDouble(42, MilLampTime);
					if (-1 != lenEngineStartAmbient)				row.SetDouble(43, EngineStartAmbient);
					if (-1 != lenEngineStartCoolant)				row.SetDouble(44, EngineStartCoolant);
					if (-1 != lenTotalDistance)						row.SetDouble(45, TotalDistance);
					if (-1 != lenTotalEngineHours)					row.SetDouble(46, TotalEngineHours);
					if (-1 != lenTotalIdleFuel)						row.SetDouble(47, TotalIdleFuel);
					if (-1 != lenTotalIdleHours)					row.SetDouble(48, TotalIdleHours);
					if (-1 != lenTotalFuel)							row.SetDouble(49, TotalFuel);
					if (-1 != lenTotalPtoFuel)						row.SetDouble(50, TotalPtoFuel);
					if (-1 != lenTripDistance)						row.SetDouble(51, TripDistance);
					if (-1 != lenTripRunTime)						row.SetDouble(52, TripRunTime);
					if (-1 != lenTripIdleTime)						row.SetDouble(53, TripIdleTime);
					if (-1 != lenIgnitionCycleCounter)				row.SetInteger(54, IgnitionCycleCounter);
					if (-1 != lenObdMonitorConditionsCounter)		row.SetInteger(55, ObdMonitorConditionsCounter);
					if (-1 != lenDm20Time)							row.SetDateTime(56, Dm20Time.year, Dm20Time.month, Dm20Time.day, Dm20Time.hour, Dm20Time.minute, Dm20Time.second, Dm20Time.fraction/1000000);
					if (-1 != lenMessageID)							row.SetInteger(57, MessageID);
					if (-1 != lenCreatedDate)						row.SetDateTime(58, CreatedDate.year, CreatedDate.month, CreatedDate.day, CreatedDate.hour, CreatedDate.minute, CreatedDate.second, CreatedDate.fraction/1000000);
					if (-1 != lenLastModified)						row.SetDateTime(59, LastModified.year, LastModified.month, LastModified.day, LastModified.hour, LastModified.minute, LastModified.second, LastModified.fraction/1000000);
					if (-1 != lenModemRSSI)							row.SetInteger(60, ModemRSSI);
					if (-1 != lenGpsHorizontalAccuracy)				row.SetDouble(61, GpsHorizontalAccuracy);
					if (-1 != lenRadioMode)							row.SetString(62, wstring(RadioMode));
					if (-1 != lenRadioStatus)						row.SetString(63, wstring(RadioStatus));
					if (-1 != lenGpsStatus)							row.SetString(64, wstring(GpsStatus));
					if (-1 != lenOverallStatus)						row.SetString(65, wstring(OverallStatus));
					if (-1 != lenGenDenominator)					row.SetInteger(66, GenDenominator);
					if (-1 != lenTripEventID)						row.SetString(67, wstring(TripEventID));
					if (-1 != lenFuelLevel)							row.SetDouble(68, FuelLevel);
					if (-1 != lenTotalEnginePtoHours)				row.SetDouble(69, TotalEnginePtoHours);
					if (-1 != lenTotalRevolutions)					row.SetInteger(70, TotalRevolutions);
					if (-1 != lenTotalTransPtoHours)				row.SetDouble(71, TotalTransPtoHours);
					if (-1 != lenVehicleSpeed)						row.SetDouble(72, VehicleSpeed);

					tablePtr->Insert(row);
				}
				retcode = SQLMoreResults(sqlStmtHandle);
				cout << "Query Successful\n";
			}

			//  Print EventID
			cout << ">>>EVENTID>>>    " << retEventID;

			//  Close ReadOnly connection
			SQLDisconnect(sqlConnHandle);

			//  Connect to SQL Server with ReadWrite
			cout << "\nAttempting ReadWrite connection to SQL Server...\n";
			retcode = SQLDriverConnectW(sqlConnHandle,
				NULL,
				(SQLWCHAR*)L"Driver={ODBC Driver 13 for SQL Server};Server=tcp:llfaq9fwjh.database.windows.net,1433;Database=paccar;Uid=vusiondba@llfaq9fwjh;Pwd=vusion!$23;Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;ApplicationIntent=ReadWrite;",
				SQL_NTS,
				retconstring,
				1024,
				NULL,
				SQL_DRIVER_NOPROMPT);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "Could not connect to SQL Server\n";
				goto COMPLETED;
			}
			else
			{
				cout << "Successfully connected to SQL Server\n";
			}

			//  Allocate statement handle
			retcode = SQLAllocHandle(SQL_HANDLE_STMT, sqlConnHandle, &sqlStmtHandle);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
			{
				cout << "SQLAllocHandle(Statement) Failed\n";
				goto COMPLETED;
			}

			//  Bind the first parameter to variable retEventID
			retcode = SQLBindParameter(sqlStmtHandle, 1, SQL_PARAM_INPUT, SQL_C_LONG, SQL_BIGINT, NULL, NULL, &retEventID, NULL, NULL);
			if ((retcode != SQL_SUCCESS) && (retcode != SQL_SUCCESS_WITH_INFO))
			{
				printf("SQLBindParameter(retEventID) Failed\n\n");
				goto COMPLETED;
			}

			//  Bind the second parameter to variable inpYearMonth
			retcode = SQLBindParameter(sqlStmtHandle, 2, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, NULL, NULL, &inpYearMonth, NULL, NULL);
			if ((retcode != SQL_SUCCESS) && (retcode != SQL_SUCCESS_WITH_INFO))
			{
				printf("SQLBindParameter(inpYearMonth) Failed\n\n");
				goto COMPLETED;
			}

			//  Execute the command
			cout << "Updating FactEventExtract...\n";
			retcode = SQLExecDirectW(sqlStmtHandle, (SQLWCHAR*)L"{CALL tde.FactEventExtract_Update (?,?)}", SQL_NTS);
			if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO && retcode != SQL_NO_DATA)
			{
				cout << "Error querying SQL Server\n";
				goto COMPLETED;
			}
			else
			{
				cout << "Query successful\n";
			}

			//  Close connection and free resources
			COMPLETED:
				SQLFreeHandle(SQL_HANDLE_STMT, sqlStmtHandle);
				SQLDisconnect(sqlConnHandle);
				SQLFreeHandle(SQL_HANDLE_DBC, sqlConnHandle);
				SQLFreeHandle(SQL_HANDLE_ENV, sqlEnvHandle);
        }
    }
    catch (const TableauException& e)
    {
        wcerr << L"A fatal error occurred while populating the extract: " << endl
                   << e.GetMessageT() << endl
                   << L"Exiting Now." << endl;
        exit(EXIT_FAILURE);
    }
}

//------------------------------------------------------------------------------
//  Main
//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    //  Parse Arguments
    map<string, wstring> options;
    if (!ParseArguments(argc - 1, argv + 1, options) || options.count("help"))
    {
        DisplayUsage();
        exit(EXIT_SUCCESS);
    }

    //  Extract API Demo
    if (options.count("build") > 0)
    {
        //  Initialize the Tableau Extract API
        ExtractAPI::Initialize();

		//  String split the yearmonth arg
		wistringstream str(options["yearmonth"]);
		wstring yearMonth;
		wstring fileName;
		while (getline(str, yearMonth, L','))
		{
			long int start = GetTickCount64();

			//  Concat yearmonth onto filename
			fileName = options["filename"] + L"_" + yearMonth + L".hyper";

			wcout << L"\n\n>>>FILENAME>>>   " << fileName;

			//  Create or Expand the Extract
			shared_ptr<Extract> extractPtr = nullptr;
			extractPtr = move(CreateExtract(fileName));
			PopulateExtract(extractPtr, stoi(yearMonth));

			//  Flush the Extract to Disk
			extractPtr->Close();

			cout << ">>>RUNTIME>>>    " << (GetTickCount64() - start) / 1000.0 << " seconds";
		}

        //  Close the Tableau Extract API
        ExtractAPI::Cleanup();
    }

    exit(EXIT_SUCCESS);
}
